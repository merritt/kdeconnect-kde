# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:32+0000\n"
"PO-Revision-Date: 2022-11-16 16:14+0700\n"
"Last-Translator: OIS <mistresssilvara@hotmail.com>\n"
"Language-Team: Interlingue <kde-i18n-doc@kde.org>\n"
"Language: ie\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.12\n"

#: backends/lan/compositeuploadjob.cpp:77
#, kde-format
msgid "Couldn't find an available port"
msgstr "Ne successat trovar un portu disponibil"

#: backends/lan/compositeuploadjob.cpp:115
#, fuzzy, kde-format
msgid "Failed to send packet to %1"
msgstr "Ne successat inviar data: %s"

#: backends/lan/compositeuploadjob.cpp:271
#, kde-format
msgid "Sending to %1"
msgstr "Inviante a %1"

#: backends/lan/compositeuploadjob.cpp:271
#, kde-format
msgid "File"
msgstr "File"

#: backends/pairinghandler.cpp:59
#, fuzzy, kde-format
msgid "Canceled by other peer"
msgstr "Anullat."

#: backends/pairinghandler.cpp:75
#, kde-format
msgid "%1: Already paired"
msgstr "%1: Ja acuplat"

#: backends/pairinghandler.cpp:84 backends/pairinghandler.cpp:98
#, fuzzy, kde-format
#| msgid "Device not reachable"
msgid "%1: Device not reachable"
msgstr "Li aparate es ínatingibil"

#: backends/pairinghandler.cpp:113
#, kde-format
msgid "Device not reachable"
msgstr "Li aparate es ínatingibil"

#: backends/pairinghandler.cpp:124
#, fuzzy, kde-format
msgid "Cancelled by user"
msgstr "Anullat."

#: backends/pairinghandler.cpp:140
#, fuzzy, kde-format
msgid "Timed out"
msgstr "Gstreamer ne ha startat transcoder"

#: compositefiletransferjob.cpp:45
#, kde-format
msgctxt "@title job"
msgid "Receiving file"
msgid_plural "Receiving files"
msgstr[0] "Recivente un file"
msgstr[1] "Recivente files"

#: compositefiletransferjob.cpp:46
#, kde-format
msgctxt "The source of a file operation"
msgid "Source"
msgstr "Orígine"

#: compositefiletransferjob.cpp:47
#, kde-format
msgctxt "The destination of a file operation"
msgid "Destination"
msgstr "Destination"

#: device.cpp:455
#, kde-format
msgid "SHA256 fingerprint of your device certificate is: %1\n"
msgstr ""

#: device.cpp:461
#, kde-format
msgid "SHA256 fingerprint of remote device certificate is: %1\n"
msgstr ""

#: filetransferjob.cpp:59
#, kde-format
msgid "Filename already present"
msgstr ""

#: filetransferjob.cpp:105
#, fuzzy, kde-format
msgid "Received incomplete file: %1"
msgstr "Vu ha recivet un file"

#: filetransferjob.cpp:123
#, kde-format
msgid "Received incomplete file from: %1"
msgstr ""

#: kdeconnectconfig.cpp:301 kdeconnectconfig.cpp:332
#, fuzzy, kde-format
msgid "KDE Connect failed to start"
msgstr "Inviar per KDE Connect"

#: kdeconnectconfig.cpp:301
#, kde-format
msgid "Could not generate the private key."
msgstr ""

#: kdeconnectconfig.cpp:317
#, kde-format
msgid "Could not store private key file: %1"
msgstr ""

#: kdeconnectconfig.cpp:332
#, fuzzy, kde-format
msgid "Could not generate the device certificate."
msgstr "Ne successat leer li file %1."

#: kdeconnectconfig.cpp:348
#, fuzzy, kde-format
msgid "Could not store certificate file: %1"
msgstr "Ne successat leer li file %1."

#~ msgid ""
#~ "This device cannot be paired because it is running an old version of KDE "
#~ "Connect."
#~ msgstr ""
#~ "Ti aparate ne posse esser acuplat pro que it have un old version de KDE "
#~ "Connect."

#~ msgid "Already paired"
#~ msgstr "Ja acuplat"
